"""
Django App for Access Redirection
"""

from __future__ import absolute_import, unicode_literals

__version__ = "1.1.0"

default_app_config = "monitoring_extensions.apps.MonitoringExtensionsConfig"  # pylint: disable=invalid-name
