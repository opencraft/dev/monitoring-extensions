"""
Django application initialization for monitoring_extensions.
"""

from django.apps import AppConfig

from monitoring_extensions.settings import REQUIRED_SETTINGS


class MonitoringExtensionsConfig(AppConfig):
    """
    Configuration for the Monitoring Insights Django App.

    Django app configuration for the plugin, which ensures the plugin
    registered correctly and required settings are set.
    """

    name = "monitoring_extensions"
    plugin_app = {
        "settings_config": {
            "lms.djangoapp": {
                "production": {
                    "relative_path": "settings",
                },
                "common": {
                    "relative_path": "settings",
                },
                "devstack": {
                    "relative_path": "settings",
                },
            },
        },
    }

    def ready(self):
        """
        Perform initialization of the Django app.

        Ensure all required settings are set for the Django app.
        """
        from django.conf import settings

        for required_setting in REQUIRED_SETTINGS:
            if not getattr(settings, required_setting, None):
                raise ValueError(
                    "{setting_key} must be set".format(setting_key=required_setting)
                )

        super().ready()
