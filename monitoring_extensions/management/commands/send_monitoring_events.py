"""
Send events to NewRelic triggered by a management command.
"""

from textwrap import dedent
from typing import Any, Optional

from django.conf import settings
from django.core.management.base import BaseCommand, CommandError, CommandParser

from monitoring_extensions.utils import get_class_from_string


class Command(BaseCommand):
    """
    Send custom events to NewRelic collected by reporters.
    """

    help = dedent(__doc__)

    def add_arguments(self, parser: CommandParser) -> None:
        parser.add_argument(
            "--dry-run",
            action="store_true",
            help="Run command without sending any events to NewRelic",
        )

        return super().add_arguments(parser)

    def handle(self, *args: Any, **options: Any) -> Optional[str]:
        enabled_reporters = {
            k: v
            for k, v in settings.MONITORING_EXTENSIONS_CONFIG.items()
            if v["enabled"] is True
        }

        for reporter_class_name, config in enabled_reporters.items():
            try:
                reporter_class = get_class_from_string(
                    config["module"],
                    reporter_class_name,
                )
            except ImportError:
                class_path = f"{config['module']}.{reporter_class_name}"
                raise CommandError('Cannot import "%s"' % class_path)
            except Exception as exc:
                raise CommandError(str(exc))

            if not options["dry_run"]:
                reporter_class().send_event()

            self.stdout.write(
                self.style.SUCCESS('Events sent for "%s"' % reporter_class_name)
            )
