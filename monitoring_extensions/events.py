"""
Pre-defined event kinds that can be used on NewRelic if enabled.
"""

import gzip
import json
import logging
from abc import abstractclassmethod, abstractproperty
from typing import Dict, Union

import redis
import requests
from django.conf import settings
from django.utils.functional import cached_property
from requests.models import Response
from requests.structures import CaseInsensitiveDict

logger = logging.getLogger(__name__)

EVENTS_ENDPOINT = f"https://{settings.NEW_RELIC_EVENT_API}/v1/accounts/{settings.NEW_RELIC_ACCOUNT_ID}/events"


def create_custom_event(data: dict) -> Response:
    """
    Create custom event on NewRelic.
    """

    headers = CaseInsensitiveDict()
    headers["Content-Type"] = "application/json"
    headers["X-Insert-Key"] = settings.NEW_RELIC_INSERT_API_KEY
    headers["Content-Encoding"] = "gzip"

    payload = gzip.compress(json.dumps(data).encode("utf-8"))
    return requests.post(EVENTS_ENDPOINT, data=payload, headers=headers)


class NewRelicReporter:
    """
    Implements the common functionality for collecting insights for monitoring.
    """

    @abstractproperty
    def event_name(self) -> str:
        """
        Return the event name for the reporter.
        """

    @abstractclassmethod
    def collect_data(self) -> Dict[str, Union[int, float, str]]:
        """
        Collect the needed information from the given resource.
        """

    def send_event(self) -> None:
        """
        Send the collected extra data to the monitoring service.
        """

        collected_data = self.collect_data()

        payload = {
            "eventType": self.event_name,
            "app_id": settings.NEW_RELIC_APPLICATION_ID,
            **collected_data,
        }

        response = create_custom_event(payload)
        if not response.ok:
            logger.error("Cannot create custom event: %s", response.content)


class CeleryTaskExecutionReporter(NewRelicReporter):
    """
    Used to report success state of a celery task.

    Compared to other reporters, this reporter should be used to indicate that
    scheduled tasks are executed, therefore nothing should be collected. Also,
    as this reporter does not collect anything, it is not part of the monitoring
    insights config, meaning it cannot be enabled/disabled by settings. To turn
    it on/off, you need to create a periodic task in celery.
    """

    def __init__(self) -> None:
        super().__init__()

    @property
    def event_name(self) -> str:
        return "CeleryTaskExecution"

    def collect_data(self) -> Dict[str, Union[int, float, str]]:
        return {}


class CeleryTaskCountReporter(NewRelicReporter):
    """
    Report the total number of tasks to process by celery workers.
    """

    def __init__(self) -> None:
        super().__init__()

        self.redis_client = None
        self.rabbitmq_client = None

    def __init_redis_connection(self) -> None:
        """
        Initialize the Redis client connection.
        """

        # The client is already initialized once
        if self.redis_client is not None:
            return

        username = settings.CELERY_BROKER_USER
        password = settings.CELERY_BROKER_PASSWORD

        host, port = settings.CELERY_BROKER_HOSTNAME.split(":")

        conn_kwargs = {}

        # Sending AUTH command with the default user/password settings can
        # cause "Client sent AUTH, but no password is set" errors
        if username != "default" or password:
            conn_kwargs.update(
                {
                    "username": username,
                    "password": password,
                }
            )

        self.redis_client = redis.Redis(
            host=host,
            port=port,
            db=settings.CELERY_BROKER_VHOST,
            ssl=settings.CELERY_BROKER_USE_SSL,
            **conn_kwargs,
        )

    @cached_property
    def redis_global_keyprefix(self) -> str:
        try:
            transport_options = settings.CELERY_BROKER_TRANSPORT_OPTIONS
        except AttributeError:
            return ""

        if not transport_options:
            return ""

        return transport_options.get('global_keyprefix', '')

    @property
    def event_name(self) -> str:
        return "CeleryTaskCount"

    def get_redis_queue_length(self, queue: str) -> int:
        """
        Return the length of the given Redis queue.
        """
        if self.redis_global_keyprefix:
            queue = f'{self.redis_global_keyprefix}{queue}'

        return self.redis_client.llen(queue)

    def collect_data(self) -> Dict[str, Union[int, float, str]]:
        if settings.CELERY_BROKER_TRANSPORT == "redis":
            retrieve_length = self.get_redis_queue_length
            self.__init_redis_connection()

        else:
            raise ValueError(f"{settings.CELERY_BROKER_TRANSPORT} is not supported")

        queues = {
            queue: retrieve_length(queue) for queue in settings.CELERY_QUEUES.keys()
        }

        return {
            **queues,
            "total": sum(queues.values()),
        }
