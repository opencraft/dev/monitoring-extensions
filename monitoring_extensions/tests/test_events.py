"""
Test event reporters.
"""

import json
from typing import Dict, Union
from unittest import TestCase
from unittest.mock import Mock, patch

from django.conf import settings
from django.test import override_settings

from monitoring_extensions.events import (
    EVENTS_ENDPOINT,
    CeleryTaskCountReporter,
    CeleryTaskExecutionReporter,
    NewRelicReporter,
    create_custom_event,
)


class FakeReporter(NewRelicReporter):
    """
    Fake reporter for testing purposes.
    """

    EXPECTED_EVENT_NAME = "FakeEvent"
    EXPECTED_COLLECTED_DATA = {"test": "data"}

    @property
    def event_name(self) -> str:
        return self.EXPECTED_EVENT_NAME

    def collect_data(self) -> Dict[str, Union[int, float, str]]:
        return self.EXPECTED_COLLECTED_DATA


class CustomEventSenderTestCase(TestCase):
    """
    Test sending custom events to NewRelic.
    """

    @patch("monitoring_extensions.events.gzip")
    @patch("monitoring_extensions.events.requests")
    def test_create_custom_event(self, mock_requests, mock_gzip):
        """
        Test creating custom event on NewRelic.
        """

        fake_api_key = "fake api key"

        data = {"test": "data"}
        encoded_data = json.dumps(data).encode("utf-8")

        expected_payload = Mock()
        mock_gzip.compress.return_value = expected_payload

        with override_settings(NEW_RELIC_INSERT_API_KEY=fake_api_key):
            create_custom_event(data)

        mock_gzip.compress.assert_called_once_with(encoded_data)
        mock_requests.post.assert_called_once_with(
            EVENTS_ENDPOINT,
            data=expected_payload,
            headers={
                "Content-Type": "application/json",
                "X-Insert-Key": fake_api_key,
                "Content-Encoding": "gzip",
            },
        )


class NewRelicReporterTestCase(TestCase):
    """
    Test reporting to NewRelic through its events API.
    """

    @patch("monitoring_extensions.events.create_custom_event")
    def test_send_event(self, mock_create_custom_event):
        """
        Test sending a custom event with the collected payload to NewRelic.
        """

        reporter = FakeReporter()

        expected_app_id = "app-id"
        expected_data = {
            "eventType": reporter.EXPECTED_EVENT_NAME,
            "app_id": expected_app_id,
            **reporter.EXPECTED_COLLECTED_DATA,
        }

        with override_settings(NEW_RELIC_APPLICATION_ID=expected_app_id):
            reporter.send_event()

        mock_create_custom_event.assert_called_once_with(expected_data)

    @patch("monitoring_extensions.events.logger")
    @patch("monitoring_extensions.events.create_custom_event")
    def test_send_event_fails(self, mock_create_custom_event, mock_logger):
        """
        Test sending a custom event to NewRelic failed.
        """

        # mimic a failed request
        expected_error_message = "something went wrong"
        mock_create_custom_event.return_value.ok = False
        mock_create_custom_event.return_value.content = expected_error_message

        reporter = FakeReporter()

        expected_app_id = "app-id"
        expected_data = {
            "eventType": reporter.EXPECTED_EVENT_NAME,
            "app_id": expected_app_id,
            **reporter.EXPECTED_COLLECTED_DATA,
        }

        with override_settings(NEW_RELIC_APPLICATION_ID=expected_app_id):
            reporter.send_event()

        mock_create_custom_event.assert_called_once_with(expected_data)
        mock_logger.error.assert_called_once_with(
            "Cannot create custom event: %s", expected_error_message
        )


class CeleryTaskExecutionReporterTestCase(TestCase):
    """
    Test celery task exection reporter.
    """

    @patch("monitoring_extensions.events.create_custom_event")
    def test_send_event(self, mock_create_custom_event):
        """
        Test sending a custom event with the collected payload to NewRelic.
        """

        reporter = CeleryTaskExecutionReporter()

        expected_app_id = "app-id"
        expected_data = {
            "eventType": "CeleryTaskExecution",
            "app_id": expected_app_id,
        }

        with override_settings(NEW_RELIC_APPLICATION_ID=expected_app_id):
            reporter.send_event()

        mock_create_custom_event.assert_called_once_with(expected_data)


class CeleryTaskCountReporterTestCase(TestCase):
    """
    Test celery task exection reporter.
    """

    @patch("monitoring_extensions.events.redis")
    @patch("monitoring_extensions.events.create_custom_event")
    def test_send_event(self, mock_create_custom_event, mock_redis):
        """
        Test sending a custom event with the collected payload to NewRelic.
        """

        mock_redis_client = Mock()
        mock_redis_client.llen.side_effect = [0, 1, 2, 3]
        mock_redis.Redis.return_value = mock_redis_client

        reporter = CeleryTaskCountReporter()

        expected_app_id = "app-id"
        expected_data = {
            "eventType": "CeleryTaskCount",
            "app_id": expected_app_id,
            "edx.lms.core.default": 0,
            "edx.lms.core.high": 1,
            "edx.lms.core.high_mem": 2,
            "edx.cms.core.default": 3,
            "total": 6,
        }

        with override_settings(NEW_RELIC_APPLICATION_ID=expected_app_id):
            reporter.send_event()

        mock_create_custom_event.assert_called_once_with(expected_data)

        host, port = settings.CELERY_BROKER_HOSTNAME.split(":")
        mock_redis.Redis.assert_called_once_with(
            host=host,
            port=port,
            db=settings.CELERY_BROKER_VHOST,
            ssl=settings.CELERY_BROKER_USE_SSL,
        )
