"""
Test celery tasks.
"""

from unittest import TestCase
from unittest.mock import Mock, patch

from django.conf import settings
from django.test import override_settings

from monitoring_extensions.tasks import monitored_celery_task, send_monitoring_events


class SendEventsTaskTestCase(TestCase):
    """
    Test sending events triggered by a celery task.
    """

    @patch("monitoring_extensions.events.redis")
    @patch("monitoring_extensions.events.create_custom_event")
    def test_send_event(self, mock_create_custom_event, mock_redis):
        """
        Test sending event is called for enabled reporters.
        """

        mock_redis_client = Mock()
        mock_redis_client.llen.side_effect = [0, 1, 2, 3]
        mock_redis.Redis.return_value = mock_redis_client

        with override_settings(ENABLE_CELERY_QUEUE_LENGTH_EVENTS=True):
            send_monitoring_events()

        mock_create_custom_event.assert_called_once_with(
            {
                "eventType": "CeleryTaskCount",
                "app_id": "fake-app",
                "edx.lms.core.default": 0,
                "edx.lms.core.high": 1,
                "edx.lms.core.high_mem": 2,
                "edx.cms.core.default": 3,
                "total": 6,
            }
        )

        host, port = settings.CELERY_BROKER_HOSTNAME.split(":")
        mock_redis.Redis.assert_called_once_with(
            host=host,
            port=port,
            db=settings.CELERY_BROKER_VHOST,
            ssl=settings.CELERY_BROKER_USE_SSL,
        )


class MonitorTaskExecutionTestCase(TestCase):
    """
    Test sending events triggered by a celery task.
    """

    @patch("monitoring_extensions.events.create_custom_event")
    def test_monitored_celery_task(
        self,
        mock_create_custom_event,
    ):
        """
        Test sending event is called for enabled reporters.
        """

        with override_settings(ENABLE_CELERY_QUEUE_LENGTH_EVENTS=True):
            monitored_celery_task()

        mock_create_custom_event.assert_called_once_with(
            {
                "eventType": "CeleryTaskExecution",
                "app_id": "fake-app",
            }
        )

class GlobalPrefixTestCase(TestCase):
    """
    Test that queue names are prefixed by the Redis global_keyprefix
    """

    @patch("monitoring_extensions.events.redis")
    @patch("monitoring_extensions.events.create_custom_event")
    def test_send_event(self, mock_create_custom_event, mock_redis):
        """
        Test sending event is called for enabled reporters.
        """

        mock_redis_client = Mock()
        mock_redis_client.llen.side_effect = [0, 1, 2, 3]
        mock_redis.Redis.return_value = mock_redis_client

        new_settings = {
            'CELERY_BROKER_TRANSPORT_OPTIONS': {'global_keyprefix': 'prefixed_'},
            'ENABLE_CELERY_QUEUE_LENGTH_EVENTS': True
        }


        with override_settings(**new_settings):
            send_monitoring_events()

        mock_redis_client.llen.assert_any_call("prefixed_edx.lms.core.default")
        mock_redis_client.llen.assert_any_call("prefixed_edx.lms.core.high")
        mock_redis_client.llen.assert_any_call("prefixed_edx.lms.core.high_mem")
        mock_redis_client.llen.assert_any_call("prefixed_edx.cms.core.default")
