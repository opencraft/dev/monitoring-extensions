"""
Test cases for utility functions.
"""

from unittest import TestCase

from monitoring_extensions.utils import get_class_from_string


class ClassImportTestCase(TestCase):
    """
    Test dynamic module and class import utilities.
    """

    def test_get_class_from_string(self):
        """
        Test import a class from a dotted path.
        """

        with self.assertRaises(NameError):
            MonitoringExtensionsConfig

        MonitoringExtensionsConfig = get_class_from_string(
            "monitoring_extensions.apps",
            "MonitoringExtensionsConfig",
        )

        self.assertIsNotNone(MonitoringExtensionsConfig)
