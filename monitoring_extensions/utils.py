"""
Utility functions.
"""

import importlib
from typing import Callable


def get_class_from_string(module_path: str, class_name: str) -> Callable:
    """
    Import a Python class dynamically and return it as a callable.
    """

    globals()[module_path] = importlib.import_module(module_path)
    imported_class = getattr(globals()[module_path], class_name)

    # Raise import error if module has no class
    if not imported_class:
        raise ImportError()

    return imported_class
