"""
Celery tasks to send monitoring events.
"""

import logging

from celery import shared_task
from django.conf import settings

from monitoring_extensions.events import CeleryTaskExecutionReporter
from monitoring_extensions.utils import get_class_from_string

logger = logging.getLogger(__name__)


@shared_task
def send_monitoring_events():
    """
    Send custom events to NewRelic collected by reporters.
    """

    enabled_reporters = {
        k: v
        for k, v in settings.MONITORING_EXTENSIONS_CONFIG.items()
        if v["enabled"] is True
    }

    for reporter_class_name, config in enabled_reporters.items():
        try:
            reporter_class = get_class_from_string(
                config["module"],
                reporter_class_name,
            )
        except ImportError:
            class_path = f"{config['module']}.{reporter_class_name}"
            raise logger.error('Cannot import "%s"' % class_path)
        except Exception as exc:
            raise logger.error(str(exc))

        reporter_class().send_event()


@shared_task
def monitored_celery_task():
    """
    Report to NewRelic when task succeeds.
    """

    reporter = CeleryTaskExecutionReporter()
    reporter.send_event()
