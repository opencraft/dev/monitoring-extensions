"""
Access Redirect Django application settings
"""

import os

REQUIRED_SETTINGS = (
    "NEW_RELIC_ACCOUNT_ID",
    "NEW_RELIC_APPLICATION_ID",
    "NEW_RELIC_INSERT_API_KEY",
)


def get_setting(settings: dict, setting_key: str, default_val=None) -> str:
    """
    Retrieves the value of the requested setting

    Gets the Value of an Environment variable either from
    the OS Environment or from the settings ENV_TOKENS

    Arguments:
        - settings (dict): Django settings
        - setting_key (str): String
        - default_val (str): String

    Returns:
        - Value of the requested setting (String)
    """

    setting_val = os.environ.get(setting_key, default_val)

    if hasattr(settings, "ENV_TOKENS"):
        return settings.ENV_TOKENS.get(setting_key, setting_val)

    return setting_val


def plugin_settings(settings: dict) -> None:
    """
    Specifies django environment settings

    Extend django settings with the plugin defined ones to be able to configure
    the plugin individually.

    Arguments:
        settings (dict): Django settings

    Returns:
        None
    """

    settings.NEW_RELIC_ACCOUNT_ID = get_setting(
        settings, "NEW_RELIC_ACCOUNT_ID", default_val=None
    )

    settings.NEW_RELIC_APPLICATION_ID = get_setting(
        settings, "NEW_RELIC_APPLICATION_ID", default_val=None
    )

    settings.NEW_RELIC_INSERT_API_KEY = get_setting(
        settings, "NEW_RELIC_INSERT_API_KEY", default_val=None
    )

    settings.NEW_RELIC_EVENT_API = get_setting(
        settings, "NEW_RELIC_EVENT_API", default_val="insights-collector.newrelic.com"
    )

    settings.ENABLE_CELERY_QUEUE_LENGTH_EVENTS = get_setting(
        settings,
        "ENABLE_CELERY_QUEUE_LENGTH_EVENTS",
        default_val=True,
    )

    settings.MONITORING_EXTENSIONS_CONFIG = {
        "CeleryTaskCountReporter": {
            "module": "monitoring_extensions.events",
            "enabled": settings.ENABLE_CELERY_QUEUE_LENGTH_EVENTS,
        }
    }
