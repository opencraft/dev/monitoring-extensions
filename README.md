# EdX app monitoring extensions

This Django Plugin App enriches the NewRelic based monitoring with custom events. The event collection is triggered by a management command, `send_monitoring_events` that's called by a cron job or by a celerybeat scheduled task, `monitoring_extensions.tasks.send_monitoring_events`.

Currently the following (configurable) events are sent to NewRelic:

- Celery queue length metrics (`CeleryTaskCount`)
- Celery (scheduled) task execution (`CeleryTaskExecution`)

## Broker support

The client supports Redis brokers. Since edX using Redis by default and OpenCraft is heading towards to Redis too, AMQP/RabbitMQ support is not implemented. However, if you need AMQP/RabbitMQ support, we are happy to receive contributions.

## Usage

1. Get necessary NewRelic [environment variables](https://docs.newrelic.com/docs/apm/agents/python-agent/configuration/python-agent-configuration/#environment-variables)
2. Install this package as a Django App
3. a) Set up a cron job to run `send_monitoring_events` management command; or
4. b) Create a new Celery beat scheduled task for `monitoring_extensions.tasks.send_monitoring_events` without any arguments

## Settings

| setting                             | description                                   | default                           |
| ----------------------------------- | --------------------------------------------- | --------------------------------- |
| `NEW_RELIC_ACCOUNT_ID`              | Account ID registered on new relic            | -                                 |
| `NEW_RELIC_APPLICATION_ID`          | User defined application identifier           | -                                 |
| `NEW_RELIC_INSERT_API_KEY`          | Insights event insert API key                 | -                                 |
| `NEW_RELIC_EVENT_API`               | Event API domain name                         | `insights-collector.newrelic.com` |
| `ENABLE_CELERY_QUEUE_LENGTH_EVENTS` | Enables monitoring of the Celery queue length | `True`                            |


## Install

First you'll need to install the django app.

```python
pip install git+https://gitlab.com/opencraft/dev/monitoring-extensions.git#egg=monitoring_extensions
```

After doing so, you'll have to enable/disable events. For example...

```python
ENABLE_CELERY_QUEUE_LENGTH_EVENTS = True
```

When the package is installed and the desired events are enabled/disabled, set up a cron job like the following, or setup a celery beat scheduled task for `monitoring_extensions.tasks.send_monitoring_events` without any arguments.

```yaml
EDXAPP_ADDITIONAL_CRON_JOBS:
    - name: "Send custom events to NewRelic"
      day: "*"
      hour: "*"
      minute: "*/5"
      user: "{{ edxapp_user }}"
      job: |
        source /edx/app/edxapp/edxapp_env && \
        /edx/bin/manage.edxapp lms send_monitoring_events"
```

At this point, the cron job can send custom events to NewRelic, but nothing alerts on an error. First of all, NewRelic documentation recommends setting an alert for `NrIntegrationError` parsing errors. To set an alert for the parsing errors, you can use the following NRQL query:

```sql
SELECT message FROM NrIntegrationError WHERE newRelicFeature = 'Event API' AND category = 'EventApiException'
```

To set alerts for custom events, a similar query could be used:

```sql
SELECT latest(total) FROM CeleryTaskCount WHERE app_id = '<APP_ID>'
```

You can also setup alerting for the following query to ensure celery beat scheduled tasks are executing:

```sql
SELECT count(*) FROM CeleryTaskExecution WHERE app_id = '<APP_ID>'
```

## Testing

### Installing Tox

The test suite uses `tox`, so install it into a virtualenv to run the tests:

```bash
pip install tox
```

### Available Tests

There are two currently available testing environments in this `tox.ini` configuration.

- `quality`
- `unit`

Each environment would default to the python version in your environment. However, you can specify one manually with the following syntax: `pyNM-testenv`. Here's an example: `py38-unit, py38-quality`

### Running Tests

#### Python Environment's Version

Run the following:

```bash
tox
```

#### Specifying Python Versions

Specify which testing environments you want to run, and with which python versions. Here's an example running python 3.8 with unit tests and just python 3.8 for quality tests.

```bash
tox -e py38-unit,py38-quality
```
