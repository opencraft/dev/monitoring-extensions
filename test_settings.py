"""
These settings are here to use during tests, because django requires them.

In a real-world use case, apps in this project are installed into other
Django applications, so these settings will not be used.
"""

ALLOWED_HOSTS = ["*"]

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.sqlite3",
        "NAME": "default.db",
        "USER": "",
        "PASSWORD": "",
        "HOST": "",
        "PORT": "",
    }
}

INSTALLED_APPS = [
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "monitoring_extensions",
]

SECRET_KEY = "insecure-secret-key"

CELERY_BROKER_USE_SSL = False
CELERY_BROKER_VHOST = 0
CELERY_QUEUES = {
    "edx.lms.core.default": {},
    "edx.lms.core.high": {},
    "edx.lms.core.high_mem": {},
    "edx.cms.core.default": {},
}
CELERY_BROKER_HOSTNAME = "127.0.0.1:6379"
CELERY_BROKER_PASSWORD = ""
CELERY_BROKER_TRANSPORT = "redis"
CELERY_BROKER_USER = "default"

NEW_RELIC_ACCOUNT_ID = 12345
NEW_RELIC_APPLICATION_ID = "fake-app"
NEW_RELIC_INSERT_API_KEY = "fake-insert-api-key"
NEW_RELIC_EVENT_API = "insights-collector.newrelic.com"

ENABLE_CELERY_QUEUE_LENGTH_EVENTS = True

MONITORING_EXTENSIONS_CONFIG = {
    "CeleryTaskCountReporter": {
        "module": "monitoring_extensions.events",
        "enabled": ENABLE_CELERY_QUEUE_LENGTH_EVENTS,
    }
}
